const Discord = require("discord.js")
const bot = new Discord.Client()
const config = require("../config.json")
const commandHandler = require("./commandHandler")
const message = require("./listeners/message")
bot.on("ready", () => {
    console.log(`${bot.user.username} is online with ${bot.guilds.get("459204790346579998").members.filter(m=> !m.user.bot).size} members`)
    commandHandler.start()
    message.listener.start(Discord,bot, commandHandler.commands)
})

bot.login(config.token)