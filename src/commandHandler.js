const Discord = require('discord.js');
const fs = require("fs");
const path = require("path");
const commands = new Discord.Collection();

const createCommandHandler = (dirname, log) => {
    let files;
    try {
        files = fs.readdirSync(dirname);
    } catch (error) {
        return console.log(error);
    }
    const jsfile = files.filter(f => f.split(".").pop() === "js")
    if (jsfile.length <= 0) {
    }else{
        console.log(log)
    }
    jsfile.forEach((f, i) => {
        const props = require(`${dirname}${f}`);
        const file = f.split(".").shift()
        const dir = dirname.split("/")
        const num = dir.length - 2
        console.log(`!.${file} loaded from ${dir[num]}`);
        commands.set(props.options.name, props);
    });
}
module.exports = {
    commands,
    start: () =>{
        const ndir = require("node-dir")
        ndir.subdirs(__dirname+"/commands/", async function(err, subdirs) {
            if (err) throw err;
            for(var i = 0; i < subdirs.length; i++){
                const dirs = subdirs[i].split("/")
                const commandDirs = dirs.splice(3, dirs.length - 2)
                createCommandHandler(path.join(__dirname, commandDirs.join("/") + "/"),`-_-_-_- ${commandDirs.pop()} folder loading... -_-_-_-`);
            }
        });
    }
}