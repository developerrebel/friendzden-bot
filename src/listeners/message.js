module.exports.listener = {
    start: (Discord, bot, commands,) => {
        const prefix = "!."
        bot.on("message", message => {
            let messageArray = message.content.split(" ");
            let cmd = messageArray[0];
            let args = messageArray.slice(1);
            let commandfile = commands.get(cmd.slice(prefix.length))
            if(!commandfile) return;
            //if(commandfile.options.globallyDisabled) return message.channel.send(errorMessages.globalDisabled).catch(err=>{})
            if(!message.member.hasPermission(commandfile.options.permissionRequirment)) return message.channel.send(errorMessages.userPermsMissing)
            if(commandfile.options.ownerOnly && message.author.id !== "284470013082075139" && message.author.id !== "326055601916608512") return message.channel.send(errorMessages.ownerOnly).catch(err=>{})
            let functionArgs = [Discord, bot, message, args, commands]
            if (commandfile) commandfile.run(...functionArgs);

        })
    }
}