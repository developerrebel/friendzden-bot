module.exports.run = async (Discord, bot, message, args, commands) => {
    let page = 1
    let infoEmbed = new Discord.MessageEmbed()
            .setTitle("Help")
            .setColor("302a99")
            .setDescription("Info category page [1/1]")
            .addField("Commands", commands.filter(c=> c.options.name !=="test" && c.options.category == "info").map(c=> c.options.helpInfo).join("\n"))
            .setFooter("To show next category press the ➡ button. To end press the ❌ button");
    
    let pages = [infoEmbed]

    message.channel.send(infoEmbed).then(async msg =>{
        msg.react("⬅")
        await msg.react("➡")
        await msg.react("❌")
        let newfilter = (reaction, user) => reaction.emoji.name === '➡' && user.id === message.author.id;
        let oldfilter = (reaction, user) => reaction.emoji.name === '⬅' && user.id === message.author.id;
        let endfilter = (reaction, user) => reaction.emoji.name === '❌' && user.id === message.author.id;
        let newPage = msg.createReactionCollector(newfilter, {time: 60000})
        let oldPage = msg.createReactionCollector(oldfilter, {time: 60000})
        let endPage = msg.createReactionCollector(endfilter, {time: 60000})
        newPage.on("end", () =>{
                msg.delete().catch(err=>{})
                message.delete().catch(err => {})
                newPage.stop()
                oldPage.stop()
                endPage.stop()
            })
        
        await newPage.on("collect", () => {
            msg.edit(pages[page])
            if(page != pages.length){
            page++
            }else{
                let error = new Discord.MessageEmbed()
                .setTitle("Error")
                .setColor("ff0000")
                .setDescription("message: You are on the last page")
                message.channel.send(error).then(msg => msg.delete({timeout: 3000}))
            }
        })
        await oldPage.on("collect", () => {
            msg.edit(pages[page - 2])
            if(page != 1){
            page--
            }else{
                let error = new Discord.MessageEmbed()
                .setTitle("Error")
                .setColor("ff0000")
                .setDescription("message: You are on the first page")
                message.channel.send(error).then(msg => msg.delete({timeout: 3000}))
            }
        })
        await endPage.on("collect", async () => {
            msg.delete()
            message.delete({timeout: 1000}).catch(err => {})
            await newPage.stop()
            await oldPage.stop()
            await endPage.stop()
        });
    })
}

module.exports.options = {
    name: "help",
    usage: "!.help",
    category: "info",
    description: "Shows information about all the command in the bot",
    helpInfo: "!.help | Shows information about all the command in the bot"
}